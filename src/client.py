import os
from subprocess import Popen, PIPE
import json
import getpass
import tempfile
import argparse
import configparser

PATH = '/var/passwords'

def get(host : str, path : str):
    content = Popen(['scp', '{host}'.format(host=host), '/dev/stdout'], stdout=PIPE)
    content, err = log.communicate()
    if err:
        return
    try:
        content = json.loads(content)
    except:
        return

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--edit', help='Éditer un fichier.', action='store_true')
    parser.add_argument('-l', '--list', help='Lister les fichiers.', action='store_true')
    parser.add_argument('-a', '--all', help='Lister tous les fichiers (y compris ceux que vous ne pouvez pas déchiffrer).', action='store_true')
    parser.add_argument('-v', '--verbosity', help='Afficher les informations de débogage', action='count')
    parser.add_argument('-H', '--host', help='Serveur à contacter', nargs='?', type=str, default='vert.adm.crans.org')
    parser.add_argument('-s', '--server', help='Emplacement du binaire du serveur.', type=str, default='passwords-server')
    parser.add_argument('path', help='Fichier à déchiffrer.', nargs=1, type=str)
    args = parser.parse_args()
    config = configparser.ConfigParser()
    if not os.path.exists(os.path.sep.join([os.getenv('HOME'), '.config'])):
        os.mkdir(os.path.sep.join([os.getenv('HOME'), '.config']))
    if not os.path.isdir(os.path.sep.join([os.getenv('HOME'), '.config'])):
        raise Exception('~/.config is not a directory.')
    if os.path.exists(os.path.sep.join([os.getenv('HOME'), '.config', 'passwords.ini'])):
        if os.path.isfile(os.path.sep.join([os.getenv('HOME'), '.config', 'passwords.ini'])):
            config.read(os.path.sep.join([os.getenv('HOME'), '.config', 'passwords.ini']))
        else:
            raise Exception('~/.config/passwords.ini is not a file.')
    ssh_args = []
    scp_args = []
    if args.verbosity:
        ssh_args.append('-' + 'v' * args.verbosity)
        scp_args.append('-' + 'v' * args.verbosity)
    else:
        args.verbosity = 0
    server_path = args.server
    target = args.host
    if args.host in config:
        root_path = config[args.host]['root_path']
        server_path = config[args.host].get('server_path', server_path)
        if 'user' in config[args.host]:
            target = '{user}@{host}'.formar(user=config[args.host]['user'], host=target)
        if 'port' in config[args.host]:
            ssh_args.append('-p {port}'.format(port=port))
            scp_args.append('-P {port}'.format(port=port))
    else:
        file = tempfile.NamedTemporaryFile()
        os.system(' '.join(['ssh', args.host, args.server, '--root', '> {tmp}'.format(tmp=file.name)]))
        file.seek(0)
        root_path = file.read().decode('utf-8').strip()
        file.close()
        config[args.host] = {}
        config[args.host]['root_path'] = root_path
        with open(os.path.sep.join([os.getenv('HOME'), '.config', 'passwords.ini']), 'w') as file:
            config.write(file)
    if args.list:
        pass
    else:
        file = tempfile.NamedTemporaryFile()
        if os.system(' '.join(['scp'] + scp_args + ['{target}:{root_path}/{path}'.format(target=target, root_path=root_path, path=args.path[0]), file.name])):
            if args.edit:
                if os.system(' '.join(['ssh'] + ssh_args + [target, "'{server_path} --root'".format(server_path=server_path), '> {tmp}'.format(tmp=file.name)])):
                    exit()
            raise Exception('Impossible de lire le fichier {path}.'.format(path=args.path[0]))
        if not args.edit:
            os.system('gpg -q --decrypt {tmp} | less -c && clear'.format(tmp=file.name))
        else:
            if not os.path.exists(os.path.sep.join([os.getenv('HOME'), '.selected_editor'])):
                while os.system('select-editor'):
                    continue
            editor = ''
            with open(os.path.sep.join([os.getenv('HOME'), '.selected_editor'])) as file:
                editor = eval(file.readlines[1][16:].strip())
            os.system('gpg --decrypt {tmp} && {editor} -'.format(tmp=file.name, editor=editor))
        file.close()

