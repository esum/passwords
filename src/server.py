import argparse
import os
import sys
import re
from enum import Enum
import grp, pwd
import getpass

class Option(Enum):
	Groups = 0
	GPG = 1

class Group(Enum):
	Unix = 0
	Internal = 1

class GPGFingerprint(Enum):
	Internal = 0


ROOT = '/var/passwords'
PATH = r'[^./]*'
GROUPS = {'user'}
OPTIONS = {
	Option.Groups: Group.Internal,
	Option.GPG: GPGFingerprint.Internal
}

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-R', '--root', help='Afficher le chemin racine du serveur.', action='store_true')
	parser.add_argument('-c', '--create', help='Créer un fichier à éditer.', type=str, default=None)
	parser.add_argument('--roles', help='Liste de rôles (séparés par des virgules) pour la création de fichiers.', type=str, default=None)
	args = parser.parse_args()
	if args.root:
		print(ROOT)
	if args.create:
		path = args.create
		if os.path.exists(os.path.sep.join([ROOT, path])):
			print('Le fichier {path} existe déjà.'.format(path=path), file=sys.stderr)
			exit(1)
		groups = set()
		if not roles:
			user = getpass.getuser()
			groups = {g.gr_name for g in grp.getgrall() if user in g.gr_mem}
			gid = pwd.getpwnam(user).pw_gid
			groups.add(grp.getgrgid(gid).gr_name)
		else:
			groups = set(args.roles.split(','))
		groups &= GROUPS
		if not groups:
			raise Exception('Vous ne pouvez pas créer de fichiers pour ces rôles.')
		if os.geteuid():
			exit(os.system('sudo {bin} --create {path} --roles {groups}'.format(bin=sys.argv[0], path=args.create, groups=','.join(list(groups)))))
		open(path, 'a').close()
		for group in groups:
			os.system('setfacl -m "g:{group}:rw-" {path}'.format(group=group, path=path))
	else:
		raise Exception('Pas de fichier spécifié.')
